function countLetter(letter, sentence) {
    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.

    if (letter.length === 1) {
        let count = 0;

        for (let i = 0; i < sentence.length; i++) {
            if (sentence[i].toLowerCase() === letter.toLowerCase()) {
                count++; 
            }
        }

        return count;
    } else {
        return undefined;
    }
}




function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.

    const lowerCaseText = text.toLowerCase();
    const charSet = new Set();

    for (let i = 0; i < lowerCaseText.length; i++) {
        const char = lowerCaseText[i];

        if (charSet.has(char)) {
            return false;
        } else {
            charSet.add(char);
        }
    }

    return true;
}





function purchase(age, price) {
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.

//answer for purchase

    if(age < 13){
        return undefined;
    }if (age >= 13 && age <= 21 || age >= 65) {

        const discountedPrice = (price * 0.80);
        const roundedPrice = discountedPrice.toFixed(2);

    } else {

        const regularPrice = Math.round(price);
        return regularPrice;
  }
};

console.log(purchase(10, 100))






function findHotCategories(items) {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

    const hotCategories = [];

    for (const item of items) {
        if (item.stocks === 0 && !hotCategories.includes(item.category)) {
            hotCategories.push(item.category);
        }
    }

    return hotCategories;
}




// const items = [
//     { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' },
//     { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' },
//     { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' },
//     { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' },
//     { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }
// ];

// console.log(findHotCategories(items));
// // Output: ['toiletries', 'gadgets']



function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    
    const setA = new Set(candidateA);
         const setB = new Set(candidateB);

          // Find the intersection of setA and setB
         const commonVoters = new Set([...setA].filter(voter => setB.has(voter)));

          // Convert the Set back to an array and return
         return Array.from(commonVoters);
        }


        const candidateA = ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m'];
        const candidateB = ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l'];

        console.log(findFlyingVoters(candidateA, candidateB));




module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};