// console.log("Hello world!");

// "document" refers to the whole page
// "querySelector" is used to select a specific object (HTML element) from the document (webpage)
const txtFirstName = document.querySelector("#txt-first-name");
const spanFullName = document.querySelector("#span-full-name");

// Alternatively, we can use the getElement functions to retrieve elements
// document.getElementById
// document.getElementByClassName
// document.getElementByTagName
// const txtFirstName = document.getElementById("txt-first-name");
// const spanFullName = document.getElementById("span-full-name");

// Whenever a user interacts with a webpage, this action is considered as an event
// "addEventListener" is a function that takes 2 arguments
// "keydown" is a string identifying an event
// Second argument - will execute one the specified is trigerred
txtFirstName.addEventListener("keyup", (event) => {
	// "innerHTML" property that sets or returns the HTML content
	spanFullName.innerHTML = txtFirstName.value;

	// ".target" contains the element where the event happened
	console.log(event.target);
	// ".value" gets the value of the input object
	console.log(event.target.value);
});


/*
// Get the first name input field element
const txtFirstName = document.querySelector("#txt-first-name");

// Get the last name input field element
const txtLastName = document.querySelector("#txt-last-name");

// Get the full name span element
const spanFullName = document.querySelector("#span-full-name");

// Function to update the full name
function updateFullName() {
  // Get the value of the first name input field
  const firstName = txtFirstName.value;

  // Get the value of the last name input field
  const lastName = txtLastName.value;

  // Concatenate the first name and last name with a space in between
  const fullName = firstName + " " + lastName;

  // Set the full name as the content of the span element
  spanFullName.innerHTML = fullName;

  // Log the event target (element) to the console
  console.log(event.target);

  // Log the value of the event target (input field) to the console
  console.log(event.target
*/